use cursive::Cursive;

use crate::{
    io::{load_state_from, save_to_dir},
    SharedAppState,
};

use super::components::{FilePicker, PaddedDialog};

/// Load new application state from a save file
///
/// Prompts the user to select a saved configureation file and attempts to
/// load the saved state into the shared application state. If an error
/// occurs, this will be displayed to the user.
///
/// # Arguments
/// * `term` - The [Cursive] instance to use for display to the user
pub fn load(term: &mut Cursive) {
    let fp = FilePicker::new(move |term, save_file| match load_state_from(save_file) {
        Ok(new_state) => {
            *term.user_data::<SharedAppState>().unwrap().lock().unwrap() = new_state;
            term.add_layer(PaddedDialog::info("App Data Loaded"))
        }
        Err(err) => term.add_layer(PaddedDialog::error(err)),
    })
    .title("Select Saved Configuration")
    .extension_filters(vec!["json".to_string()]);

    match fp.load(dirs::home_dir().unwrap().as_path()) {
        Ok(file_picker) => term.add_layer(file_picker),
        Err(e) => term.add_layer(PaddedDialog::error(e)),
    }
}

/// Save the current application state to a file
///
/// Prompts the user to select a directory in which to save the current
/// shared application state. If an error occurs, this will be displayed
/// to the user.
///
/// # Arguments
/// * `term` - The [Cursive] instance to use for display to the user
pub fn save(term: &mut Cursive) {
    let app_state = term.user_data::<SharedAppState>().unwrap().clone();
    let fp = FilePicker::new(move |term, save_dir| {
        match save_to_dir(save_dir, &app_state.lock().unwrap()) {
            Ok(file) => term.add_layer(PaddedDialog::info(format!(
                "App Data Saved to\n{}!",
                file.to_str().unwrap()
            ))),
            Err(err) => term.add_layer(PaddedDialog::error(err)),
        }
    })
    .title("Select a Save Location")
    .dirs_only(true);

    match fp.load(dirs::home_dir().unwrap().as_path()) {
        Ok(file_picker) => term.add_layer(file_picker),
        Err(e) => term.add_layer(PaddedDialog::error(e)),
    }
}
