pub mod components;
pub mod io;

use cursive::{
    event::{Event, Key},
    menu,
    views::TextView,
    Cursive, CursiveExt,
};

use crate::{model::collection::Collection, SharedAppState};

use self::{components::PaddedDialog, io::*};

/// A TUI interface for the application
pub struct TerminalGUI {
    pub app_state: SharedAppState,
}

impl TerminalGUI {
    /// Start and display the GUI to the user
    pub fn run(self) {
        let mut term = Cursive::new();
        term.set_user_data(self.app_state);
        build_main_menu(&mut term);
        term.run();
    }
}

/// Construct the main menu for the GUI
///
/// # Arguments
/// * `term` - The [Cursive] instance to add the menus to
fn build_main_menu(term: &mut Cursive) {
    term.menubar()
        .add_subtree(
            "File",
            menu::Tree::new()
                .leaf("Load Settings", load)
                .leaf("Save Settings", save)
                .leaf("Quit (Ctrl-q)", |term| term.quit()),
        )
        .add_subtree(
            "Non-Mame",
            menu::Tree::new()
                .leaf("List Collections", display_non_mame_collections)
                .leaf("Add Collection", add_non_mame_collection),
        );
    term.set_autohide_menu(false);
    term.set_global_callback(Key::Esc, |term| term.select_menubar());
    term.set_global_callback(Event::CtrlChar('q'), |term| term.quit());
}

fn display_non_mame_collections(term: &mut Cursive) {
    let panel = PaddedDialog::around(TextView::new(format!(
        "{:?}",
        term.user_data::<SharedAppState>()
            .unwrap()
            .lock()
            .unwrap()
            .non_mame_manager
            .collections
    )))
    .title("Non Mame Collections");

    term.add_layer(panel);
}

fn add_non_mame_collection(term: &mut Cursive) {
    let collection = Collection::new();
    term.user_data::<SharedAppState>()
        .unwrap()
        .lock()
        .unwrap()
        .non_mame_manager
        .collections
        .push(collection);
    term.add_layer(PaddedDialog::info("New Unnamed Collection Added!"));
}
