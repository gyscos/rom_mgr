use std::{
    error::Error,
    fs::{read_dir, DirEntry},
    path::{Path, PathBuf},
    rc::Rc,
    sync::{Arc, Mutex},
};

use crate::enclose;
use crate::AppError;
use cursive::{
    traits::{Finder, Nameable},
    view::ViewWrapper,
    views::{Button, DummyView, LinearLayout, NamedView, Panel, ScrollView, SelectView, TextView},
    wrap_impl, Cursive, With,
};
use cursive_aligned_view::Alignable;

use super::padded_dialog::PaddedDialog;

#[derive(Clone)]
struct FilterOptions {
    dirs_only: bool,
    no_hidden: bool,
    extensions: Vec<String>,
}

impl Default for FilterOptions {
    fn default() -> Self {
        FilterOptions {
            dirs_only: false,
            no_hidden: true,
            extensions: Vec::default(),
        }
    }
}

//#[derive(Default)]
pub struct FilePicker {
    filters: Arc<Mutex<FilterOptions>>,
    view: Panel<LinearLayout>,
}

type ScrollingSelector = ScrollView<NamedView<SelectView<PathBuf>>>;

impl FilePicker {
    const PICKER_NAME: &'static str = "file_selector";
    const CURRENT_DIR_NAME: &'static str = "current_dir";

    pub fn new<F>(on_pick: F) -> Self
    where
        F: Fn(&mut Cursive, &Path) + 'static,
    {
        let filters = Arc::new(Mutex::new(FilterOptions::default()));
        let on_pick = Rc::new(on_pick);
        let on_pick_ok = on_pick.clone();
        let selector: ScrollingSelector = ScrollView::new(
            SelectView::new()
                .on_submit(enclose! {(filters) move |term, entry: &PathBuf| {
                    handle_selected_entry(entry, term, &filters.lock().unwrap(), on_pick.clone());
                }})
                .with_name(Self::PICKER_NAME),
        );

        let buttons = LinearLayout::horizontal()
            .child(Button::new("Cancel", |term| {
                term.pop_layer();
            }))
            .child(DummyView)
            .child(Button::new("Ok", move |term| {
                let selector = term
                    .find_name::<SelectView<PathBuf>>(Self::PICKER_NAME)
                    .unwrap();
                let path = selector.selection().unwrap();
                term.pop_layer();
                on_pick_ok(term, &path);
            }))
            .align_center();

        let view = Panel::new(
            LinearLayout::vertical()
                .child(
                    LinearLayout::horizontal()
                        .child(TextView::new("Viewing: "))
                        .child(TextView::new(String::default()).with_name(Self::CURRENT_DIR_NAME)),
                )
                .child(Panel::new(selector))
                .child(DummyView)
                .child(buttons),
        )
        .title("Select a File");

        FilePicker { filters, view }
    }

    pub fn dirs_only(self, dirs_only: bool) -> Self {
        self.with(|s| s.filters.lock().unwrap().dirs_only = dirs_only)
    }

    pub fn load(mut self, path: &Path) -> Result<Self, AppError> {
        let filters = self.filters.clone();
        let result = match read_dirs(path, &filters.lock().unwrap()) {
            Ok(paths) => {
                let mut selector = self
                    .find_name::<SelectView<PathBuf>>(Self::PICKER_NAME)
                    .unwrap();
                selector.add_all(paths);
                let mut viewing = self.find_name::<TextView>(Self::CURRENT_DIR_NAME).unwrap();
                viewing.set_content(path.to_str().unwrap());
                Ok(())
            }
            Err(err) => Err(err),
        };

        result.map(|_| self)
    }

    pub fn title<T>(self, title: T) -> Self
    where
        T: Into<String>,
    {
        self.with(|s| s.view.set_title(title))
    }

    pub fn extension_filters(self, extensions: Vec<String>) -> Self {
        self.with(|s| s.filters.lock().unwrap().extensions = extensions)
    }
}

fn handle_selected_entry<F>(
    entry: &Path,
    term: &mut Cursive,
    filters: &FilterOptions,
    on_pick: Rc<F>,
) where
    F: Fn(&mut Cursive, &Path),
{
    if entry.is_dir() {
        let mut selector = term
            .find_name::<SelectView<PathBuf>>("file_selector")
            .unwrap();
        let mut current = term
            .find_name::<TextView>(FilePicker::CURRENT_DIR_NAME)
            .unwrap();
        match read_dirs(entry, filters) {
            Ok(paths) => {
                selector.clear();
                selector.add_all(paths);
                current.set_content(entry.to_str().unwrap());
            }
            Err(err) => term.add_layer(PaddedDialog::error(err)),
        }
    } else {
        term.pop_layer();
        on_pick(term, entry);
    }
}

/// Returns an iterator over the contents of a given path according to filters
///
/// # Arguments
/// * `path`    - The path to read the contents of
/// * `filters` - The filters to apply when reading
fn read_dirs(
    path: &Path,
    filters: &FilterOptions,
) -> Result<impl Iterator<Item = (String, PathBuf)>, AppError> {
    match read_dir(path) {
        Ok(paths) => {
            // Get all items from the path
            let mut sorted: Vec<PathBuf> = paths.filter_map(|e| dir_filter(e, filters)).collect();
            sorted.sort_by_key(|path| path.file_name().unwrap().to_str().unwrap().to_owned());

            let mut items: Vec<(String, PathBuf)> = sorted
                .into_iter()
                .map(|entry| {
                    let name = entry.file_name().unwrap().to_str().unwrap().to_owned();
                    (name, entry)
                })
                .collect();

            // Add an etry for the current path
            items.insert(0, (". (This Directory)".to_string(), path.to_path_buf()));

            // Add an entry for the parent if present
            if path.parent().is_some() {
                items.insert(
                    0,
                    (
                        "\u{021A9} Parent".to_string(),
                        path.parent().unwrap().to_path_buf(),
                    ),
                )
            }

            Ok(items.into_iter())
        }
        Err(error) => Err(error.into()),
    }
}

fn dir_filter(entry: Result<DirEntry, impl Error>, filters: &FilterOptions) -> Option<PathBuf> {
    if entry.is_err() {
        return Option::None;
    }
    let entry = entry.unwrap().path();

    if filters.no_hidden
        && entry
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .starts_with('.')
    {
        return Option::None;
    }

    if entry.is_dir() {
        // After filtering for hidden, always include directories
        return Some(entry);
    }

    if filters.dirs_only {
        return Option::None;
    }

    if !filters.extensions.is_empty() {
        match entry.extension() {
            Some(ext) => {
                if !filters
                    .extensions
                    .iter()
                    .any(|fext| ext.eq_ignore_ascii_case(fext))
                {
                    return Option::None;
                }
            }
            None => return Option::None,
        }
    }

    Some(entry)
}

impl ViewWrapper for FilePicker {
    wrap_impl!(self.view: Panel<LinearLayout>);
}
