use crate::AppError;
use cursive::{
    align::HAlign,
    event::Key,
    view::{IntoBoxedView, Margins, ViewWrapper},
    views::{Dialog, OnEventView},
    wrap_impl, Cursive, With,
};

pub struct PaddedDialog {
    dialog: OnEventView<Dialog>,
}

impl ViewWrapper for PaddedDialog {
    wrap_impl!(self.dialog: OnEventView<Dialog>);
}

impl PaddedDialog {
    pub fn around<V>(view: V) -> Self
    where
        V: IntoBoxedView,
    {
        Self {
            dialog: OnEventView::new(
                Dialog::around(view)
                    .padding(Margins::lrtb(1, 1, 1, 1))
                    .dismiss_button("OK")
                    .h_align(HAlign::Center),
            )
            .on_event(Key::Esc, Self::dismiss_dialog),
        }
    }

    pub fn info<S>(text: S) -> Self
    where
        S: Into<String>,
    {
        Self {
            dialog: OnEventView::new(
                Dialog::info(text)
                    .padding(Margins::lrtb(1, 1, 1, 1))
                    .h_align(HAlign::Center),
            )
            .on_event(Key::Esc, Self::dismiss_dialog),
        }
    }

    pub fn error<E>(error: E) -> Self
    where
        E: Into<AppError>,
    {
        Self {
            dialog: OnEventView::new(
                Dialog::info(format!("{}", error.into()))
                    .padding(Margins::lrtb(1, 1, 1, 1))
                    .h_align(HAlign::Center),
            )
            .on_event(Key::Esc, Self::dismiss_dialog),
        }
    }

    pub fn title<S>(self, title: S) -> Self
    where
        S: Into<String>,
    {
        self.with(|s| s.dialog.get_inner_mut().set_title(title))
    }

    fn dismiss_dialog(term: &mut Cursive) {
        term.pop_layer();
    }
}
