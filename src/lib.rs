pub mod gui;
pub mod io;
pub mod model;

use std::{
    error::Error,
    fmt::Display,
    sync::{Arc, Mutex},
};

use model::non_mame_manager::NonMameManager;
use serde::{Deserialize, Serialize};

type SharedAppState = Arc<Mutex<AppState>>;
#[derive(Debug, Serialize, Deserialize)]
pub struct AppState {
    pub non_mame_manager: NonMameManager,
}

#[derive(Debug)]
pub enum AppError {
    StringError { cause: String },
    BoxError { cause: Box<dyn Error> },
}

impl Error for AppError {}

impl Display for AppError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::StringError { cause } => cause.to_owned(),
            Self::BoxError { cause } => format!("{}", cause),
        };
        f.write_str(&message)
    }
}

impl From<serde_json::Error> for AppError {
    fn from(sj: serde_json::Error) -> Self {
        AppError::BoxError {
            cause: Box::new(sj),
        }
    }
}

impl From<std::io::Error> for AppError {
    fn from(io: std::io::Error) -> Self {
        AppError::BoxError {
            cause: Box::new(io),
        }
    }
}

impl From<&str> for AppError {
    fn from(message: &str) -> Self {
        AppError::StringError {
            cause: message.to_string(),
        }
    }
}

#[macro_export]
macro_rules! enclose {
    ( ($( $x:ident ),*) $y:expr ) => {
        {
            $(let $x = $x.clone();)*
            $y
        }
    };
}
