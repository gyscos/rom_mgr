use std::{
    panic::{self, PanicInfo},
    sync::{Arc, Mutex},
};

use rom_mgr::{gui::TerminalGUI, model::non_mame_manager::NonMameManager, AppState};

fn panic_handler(pi: &PanicInfo) {
    eprintln!("{}", pi);
}

fn main() {
    panic::set_hook(Box::new(panic_handler));

    let app_state = AppState {
        non_mame_manager: NonMameManager::default(),
    };

    let app_state = Arc::new(Mutex::new(app_state));

    let gui = TerminalGUI { app_state };
    gui.run();
}
