use self::non_mame_manager::NonMameManager;

pub mod collection;
pub mod non_mame_manager;

pub struct AppState {
    pub non_mame_manager: NonMameManager,
}
