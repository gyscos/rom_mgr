use std::{
    fs::File,
    path::{Path, PathBuf},
};

use crate::{AppError, AppState};

/// Load application state from a file on disk
///
/// # Arguments
/// * `save_file` - The path to the file on disk to load from
///
/// # Errors
/// An error will be returned if the selected file does not have the `.json`
/// extension, or if there is an error parsing the JSON in the file
pub fn load_state_from(save_file: &Path) -> Result<AppState, AppError> {
    if !(save_file.extension().is_some()
        && save_file.extension().unwrap().to_ascii_lowercase() == "json")
    {
        return Err("Save files must be JSON".into());
    }
    let file = File::open(save_file)?;
    let new_state: AppState = serde_json::from_reader(file)?;
    Ok(new_state)
}

/// Save application state to a directory.
///
/// The state will be saved to  file named `rom_mgr-config.json`
///
/// # Arguments
/// * `save_dir`  - A path to a directory in which to create the save file
/// * `app_state` - The application state to save
///
/// # Errors
/// An error will be returned if there are any errors creating or writing to
/// the save file.
pub fn save_to_dir(save_dir: &Path, app_state: &AppState) -> Result<PathBuf, AppError> {
    let mut save_file = save_dir.to_path_buf();
    save_file.push("rom_mgr-config.json");
    let file = File::create(save_file.as_path())?;
    serde_json::to_writer_pretty(file, app_state)?;
    Ok(save_file)
}
